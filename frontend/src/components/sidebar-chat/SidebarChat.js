import React, { useState, useEffect } from 'react'
import Avatar from '@mui/material/Avatar'
import './SidebarChat.css'

const SidebarChat = () => {

    return (
        <div className='sidebarChat'>
            <Avatar src='https://cdn.vectorstock.com/i/preview-2x/30/97/flat-business-man-user-profile-avatar-icon-vector-4333097.webp'/>

            <div className='sidebarChat__info'>
                <h2>Room name</h2>
                <p>Last message...</p>
            </div>
        </div>
    )
}

export default SidebarChat
