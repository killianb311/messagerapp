import React from 'react';
import './Sidebar.css';
import { DonutLarge, Chat, MoreVert } from '@mui/icons-material';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import { SearchOutlined } from '@mui/icons-material';
import SidebarChat from '../sidebar-chat/SidebarChat';

function Sidebar() {
  return (
    <div className="sidebar">
      <div className='sidebar__header'>
        <Avatar src='https://cdn.vectorstock.com/i/preview-2x/06/66/brown-hair-businessman-avatar-man-face-profile-vector-21960666.webp' />
        <div>
          <IconButton>
            <DonutLarge />
          </IconButton>

          <IconButton>
            <Chat />
          </IconButton>

          <IconButton>
            <MoreVert />
          </IconButton>
        </div>
      </div>
      <div className='sidebar__search'>
        <div className='sidebar__searchContainer'>
          <SearchOutlined />

          <input type='text' placeholder='Search to star a new chat' />
        </div>
      </div>
      <div className='sidebar__chats'>
        <SidebarChat />
        <SidebarChat />
        <SidebarChat />
      </div>
    </div>
  );
}

export default Sidebar;